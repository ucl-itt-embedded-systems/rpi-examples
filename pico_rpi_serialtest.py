import serial
from time import sleep

try:
	with serial.Serial('/dev/serial0', 9600) as ser:
		while(True):
			x = ser.readline()
			x = x.decode('utf-8')
			x = x[1:-2]
			print(f'type: {type(x)}, message: {x}')
			sleep(0.5)
except KeyboardInterrupt:
	print('program closed.....')

